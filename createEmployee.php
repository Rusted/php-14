<?php
require_once 'functions.php';

$pdo = getConnection();
$positions = getAllPositions($pdo);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    //todo - field validation
    $result = createEmployee(
        $pdo,
        $_POST['name'],
        $_POST['surname'],
        $_POST['gender'],
        $_POST['birthday'],
        $_POST['education'],
        (int) $_POST['salary'],
        (int) $_POST['idarbinimo_tipas'],
        (int) $_POST['pareigos_id']
    );

    if ($result) {
        header('Location:index.php');
        exit();
    }
}

?>
<html>
<head>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<h1>Naujas darbuotojas </h1>
<form method="post">
    <div>
        <label>Name</label>
        <input type="text" name="name" />
        <div />
        <div>
            <label>Surname</label>
            <input type="text" name="surname" />
        </div>
        <div>
            <label>Gender</label>
            <select name="gender">
                <option value="vyras">Vyras</option>
                <option value="moteris">Moteris</option>
            <select/>
        </div>
        <div>
            <label>Birthday</label>
            <input type="text" name="birthday" class="datepicker" />
        </div>
        <div>
            <label>Education</label>

            <input type="text" name="education" />
        </div>
        <div>
            <label>Salary</label>

            <input type="text" name="salary" />
        </div>
        <div>
            <label>idarbinimo_tipas</label>
            <select name="idarbinimo_tipas">
                <option value="1">Paprastas</option>
                <option value="2">Kontraktas</option>
            <select/>
        </div>
        <div>
            <label>Pareigos id</label>
            <select name="pareigos_id">
            <?php foreach ($positions as $position) {?>
                <option value="<?php echo $position['id']; ?>"><?php echo $position['name']; ?></option>
            <?php }?>
            </select>
        </div>
        <div>
        <input type="submit" />
        </div>
</form>
<script>
  $( function() {
    $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
  } );
</script>

</html>
