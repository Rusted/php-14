<?php declare (strict_types = 1);

const HOST = 'localhost';
const DB_USER = 'root';
const DB_PASSWORD = 'lunatikas';
const DATABASE = 'baltic_talents';

function getConnection(): PDO
{
    $pdo = new PDO('mysql:host=' . HOST . ';dbname=' . DATABASE . ';charset=utf8mb4', DB_USER, DB_PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    return $pdo;
}

function createEmployee(PDO $pdo, string $name, string $surname, string $gender, string $birthday, string $education, int $salary, int $idarbinimoTipas, int $pareigosId): bool
{
    $sql = 'INSERT into darbuotojai(name, surname, gender, birthday, education, salary, idarbinimo_tipas, pareigos_id)
     VALUES (:name, :surname, :gender, :birthday, :education, :salary,  :idarbinimo_tipas, :pareigos_id)';

    $query = $pdo->prepare($sql);
    return $query->execute([
        'name' => $name,
        'surname' => $surname,
        'gender' => $gender,
        'birthday' => $birthday,
        'education' => $education,
        'salary' => $salary,
        'idarbinimo_tipas' => $idarbinimoTipas,
        'pareigos_id' => $pareigosId,
    ]);
}

function updateEmployee(PDO $pdo, int $id, string $name, string $surname, string $gender, string $birthday, string $education, int $salary, int $idarbinimoTipas, int $pareigosId): bool
{
    $sql = 'UPDATE darbuotojai
    SET name=:name,
        surname=:surname,
        gender=:gender,
        birthday=:birthday,
        education=:education,
        salary=:salary,
        idarbinimo_tipas=:idarbinimo_tipas,
        pareigos_id=:pareigos_id
    WHERE id=:id';

    $query = $pdo->prepare($sql);

    return $query->execute([
        'id' => $id,
        'name' => $name,
        'surname' => $surname,
        'gender' => $gender,
        'birthday' => $birthday,
        'education' => $education,
        'salary' => $salary,
        'idarbinimo_tipas' => $idarbinimoTipas,
        'pareigos_id' => $pareigosId,
    ]);
}

function getAllPositions(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT id, name, base_salary FROM pareigos');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getPosition(PDO $pdo, int $id)
{
    $stmt = $pdo->prepare('SELECT id, name, base_salary FROM pareigos WHERE id=:id');
    $stmt->execute(['id' => $id]);

    return $stmt->fetch();
}

function createPosition(PDO $pdo, string $name, string $salary): bool
{
    $sql = 'INSERT into pareigos(name, base_salary)
     VALUES (:name, :base_salary)';

    $query = $pdo->prepare($sql);
    return $query->execute([
        'name' => $name,
        'base_salary' => $salary,
    ]);
}

function updatePosition(PDO $pdo, int $id, string $name, int $baseSalary)
{
    $sql = 'UPDATE pareigos SET name=:name, base_salary=:base_salary WHERE id=:id';
    $stmt = $pdo->prepare($sql);
    $stmt->execute(['id' => $id, 'name' => $name, 'base_salary' => $baseSalary]);
}

function getEmployee(PDO $pdo, int $employeeId): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone, birthday, pareigos_id, idarbinimo_tipas FROM darbuotojai WHERE id =:id');
    $stmt->execute(['id' => $employeeId]);
    return $stmt->fetch();
}

function getAllEmployees(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT id, name, surname, education, salary, phone FROM darbuotojai');
    $stmt->execute();

    return $stmt->fetchAll();
}

function getEmployeeCounts(PDO $pdo): array
{
    $stmt = $pdo->prepare('SELECT pareigos_id, count(*) employee_count FROM darbuotojai GROUP BY pareigos_id');
    $stmt->execute();

    return $stmt->fetchAll(PDO::FETCH_KEY_PAIR);
}

function deletePosition(PDO $pdo, int $id): bool
{
    $stmt = $pdo->prepare('DELETE FROM pareigos WHERE id=:id');
    return $stmt->execute(['id' => $id]);
}

function deleteEmployee(PDO $pdo, int $id): bool
{
    $stmt = $pdo->prepare('DELETE FROM darbuotojai WHERE id=:id');
    return $stmt->execute(['id' => $id]);
}
